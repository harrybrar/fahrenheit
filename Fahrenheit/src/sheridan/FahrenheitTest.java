package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	//REgular
	@Test
	public void testIsNotCelsuis() {
		assertNotEquals(25, (int)Fahrenheit.convertFromCelsius(25));
	}
	
	//Negative
	@Test
	public void testIsCelsuis() {
		assertEquals(32, (int)Fahrenheit.convertFromCelsius(0));
	}
	
	//TestIsBoundary
	@Test
	public void testIsBoundaryCelsuis() {
		assertNotEquals(33, (int)Fahrenheit.convertFromCelsius(0));
	}
	
	//TestIsBoundary
	@Test
	public void testIsBoundaryCelsuis2() {
		assertNotEquals(31, (int)Fahrenheit.convertFromCelsius(0));
	}
}
